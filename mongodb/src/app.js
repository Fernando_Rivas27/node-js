const bodyParser=require('body-parser');
const morgan=require('morgan');
const express=require('express');
const app=express();
const mongoose=require('mongoose');
const userROuters=(require('./routes/user'));
//config
app.set('port',3000);

mongoose.Promise=global.Promise;

mongoose.connect(
    'mongodb://localhost:27017/rest-api-example',
     { useNewUrlParser: true } ,
     console.log("holaaaaaa")
).then( ()=> console.log('Se conecto a la Base de datos')).catch( ()=>console.log("Se detuvo en esta parte"));

//midleware
app.use(bodyParser.json());
app.use(morgan('dev'));

//Routes
app.use('/users',userROuters);

//Listen Server
app.listen(app.get('port'),()=>
 {
    console.log('Servidor en el puerto ',app.get('port'));
}
);
