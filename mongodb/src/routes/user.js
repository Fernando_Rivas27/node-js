
const router=require('express-promise-router')();


const { index,newUsercars, newUser,getUserCars ,getUser,replaceUser,deleteUser}=require('../controllers/users');

router.get('/',index);
router.get('/:userId',getUser);
router.put('/:userId',replaceUser);
router.post('/',newUser);
router.delete('/:userId',deleteUser),
router.get('/:userId/cars',getUserCars);
router.post('/:userId/cars',newUsercars);


module.exports=router;
